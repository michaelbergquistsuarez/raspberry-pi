# Raspberry Pi
Instructions for setting up my Raspberry Pi development environment.

## Introduction
This guide was written for a Raspberry Pi 3.
I created this for my own sake. But hey, feel free to clone or improve.
I'll begin with simple instructions. I might add some automatisation scripts later on.

## What you need
1. A Raspberry Pi
1. A Sim Card (with at least 4 GB of storage)

## Prepare OS
1. Download and install [Raspbian Jessie Lite](https://downloads.raspberrypi.org/raspbian_lite_latest) on the SD card.
1. Insert the bootable SD card and start the Raspberry.
1. Log in with `pi` `raspberry`
1. Plug in a ethernet cable
1. `bash -c "$(curl -fsSL https://raw.githubusercontent.com/michaelbergquistsuarez/raspberry-pi/master/tools/install.sh)"`

## Continue within SSH
You may now access the Raspberry Pi with SSH from another host.
I recommend you do that.

1. `ssh pi@127.0.0.1` (where `127.0.0.1` is an IP adress to the unit). Make sure port `22` isn't blocked along the way.

## Additional setup
1. Change factory password `sudo passwd pi`. `logout` so that you can make sure you entered the right one.
1. Enter `raspi-config` and expand the filesystem.
1. `apt-get update` `apt-get upgrade` `apt-get check`

### Setup SSH keys
1. `ssh-keygen` and follow instructions.
1. Remember where the public key was created.

## Install and prepare software

### git
Version handling

#### Usage

##### Setting up a repository on server
These instructions are for GitHub. You don't have to push to a remote repository. You can keep everything local if you want to.

1. Log in to GitHub
1. Make sure your unit has its *public* ssh keys [added](https://github.com/settings/keys)
1. Create a repository that we'll connect the client repo to.

##### Setting up a repository on client
1. `mkdir repo-name` `cd repo-name`
1. `git init` and follow instructions.
1. If you are not using a remote repo, you may skip these instructions
    1.  `git remote add origin remote repository "https://github.com/MichaelBergquistSuarez/raspberry-pi.git"` where the URL is the URL to your repo.
    1. Verify the remote URL `git remote -v`

##### Commit to a repository
1. Make changes in your files.
1. `git add .`
1. `git commit -m "My commit"`
1. If using a remote repository, push the changes to it `git push origin master` 

# Installed software
1. git
1. tmux
1. vim

## Setup, round 2
1. Copy all files but this README and put it in your home directory
1. `mkdir ~/.vim/tmp; ~/.vim/tmp/backup; ~/.vim/tmp/undo; ~/.vim/tmp/swap;`
1. `vim` and run vim command `:PlugInstall`


# References
1. https://github.com/asb/raspi-config
1. http://linux.die.net/man/8/wpa_cli
1. Check this dude out. Seem to have similar ideas and concepts as me. https://gist.github.com/martijnvermaat/7864115

# To do
1. Keyboard settings should not be copied from a file, but added programmatically.
1. Wi-Fi support for multiple networks (add instead of assuming id 0)
1. Instructions for how to install and run install script from SD card (100% headless)
1. Non-interactive mode
