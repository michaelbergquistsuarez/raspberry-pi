# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh/histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob nomatch notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/pi/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

#setopt autopushd pushdignoredups # Make cd act like pushd

PATH="$HOME/bin:$PATH" # Uh. Saw this at https://github.com/tallus/dot-files/blob/master/zshrc looks useful. I think.

# Autorun tmux if not already ran
#tmux
bash ~/.autorun/welcome.sh # Welcoming script to explain stuff
