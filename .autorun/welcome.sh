#!/bin/bash
# Remember, this script must be run with `bash shell.sh`
# In vim? Run with `:w | :call VimuxRunCommand("bash shell.sh")

echo "Hi $USER."

SSH_CLIENT_PARTS=($SSH_CLIENT)

#echo "The SSH client array is composed of ${#SSH_CLIENT_PARTS[@]} parts."
if [ "${#SSH_CLIENT_PARTS[@]}" ==  3 ]; then
    echo "You are running from ${SSH_CLIENT_PARTS[0]} at port ${SSH_CLIENT_PARTS[1]}"
    echo "The server is responding at port ${SSH_CLIENT_PARTS[2]}"
else
    echo "Can't interpret SSH client variable; \"$SSH_CLIENT\""
fi
