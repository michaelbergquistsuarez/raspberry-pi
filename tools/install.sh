#!/bin/bash

# Make sure the user has done a sudo
if [ $EUID -ne 0 ]; then
    echo "Please run this script as root"
    exit
fi

# Ask user to confirm reboot
while :
do
  echo "This script will reboot the system after completion. Continue?"
  echo "Yes (1)"
  echo "No (2)"

  read -n1 -s
  case "$REPLY" in
    "1") break;;
    "2") echo "The installer was closed without making any changes."; exit;;
    *) printf "\nInvalid option\n";;
  esac
done

# Git setup
function gitsetup {
  read -p "Please enter your full name for your git environment: " GIT_NAME
  read -p "Please enter your e-mail adress for your git environment: " GIT_MAIL
}

gitsetup
while :
do
  echo "Configure git with name \"$GIT_NAME\" and e-mail \"$GIT_MAIL\"?"
  echo "Yes (1)"
  echo "No (2)"
  
  read -n1 -s
  case "$REPLY" in
    "1") break;;
    "2") gitsetup;;
    *) printf "\nInvalid option\n";;
  esac
done

# Wi-Fi setup
function wifisetup {
  read -p "Wi-Fi network name: " WIFI_NAME
  read -p "Wi-Fi network password: " WIFI_PSK
}

wifisetup
while :
do
  echo "Configure Wi-Fi with name \"$WIFI_NAME\" and password \"$WIFI_PSK\"?"
  echo "Yes (1)"
  echo "No (2)"

  read -n1 -s
  case "$REPLY" in
    "1") break;;
    "2") wifisetup;;
    *) printf "\nInvalid option\n"
  esac
done

# Configure Wi-Fi. We assume this has never been done before
wpa_cli add_network # Assume ID 0

echo "Setting Wi-Fi network \"$WIFI_NAME\""
wpa_cli set_network 0 ssid "\"$WIFI_NAME\""

echo "Setting Wi-Fi password \"$WIFI_PSK\""
wpa_cli set_network 0 psk "\"$WIFI_PSK\""

wpa_cli enable_network 0
wpa_cli save_config

exit

# Expand filesystem. Note that warnings about the device being busy seem normal
echo "Expanding filesystem"
raspi-config --expand-rootfs

echo "Setting up environment"
echo "The system will need to restart before the changes will take effect."

cd "$(dirname $0)"/..
echo "Current working directory is now $PWD"

sudo cp files/etc/default/keyboard /etc/default/keyboard
echo "Installed swedish keyboard layout"

echo "Installing software"
apt-get install git
apt-get install tmux
apt-get install vim

echo "Configuring git"
git config --global user.name "$GIT_NAME"
git config --global user.email "$GIT_MAIL"

echo "The system will now restart. This connection will be lost.";
echo "Please try reconnecting again in a moment so that the system may boot."

#sudo shutdown -r now
